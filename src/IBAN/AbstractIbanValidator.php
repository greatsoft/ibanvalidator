<?php

namespace Validator\IBAN;

use LogicException;

abstract class AbstractIbanValidator implements IbanValidatorInterface
{
    /**
     * Country letters codes
     * @var array
     */
    private const LETTERS_CODES = [
        'A' => 10,
        'B' => 11,
        'C' => 12,
        'D' => 13,
        'E' => 14,
        'F' => 15,
        'G' => 16,
        'H' => 17,
        'I' => 18,
        'J' => 19,
        'K' => 20,
        'L' => 21,
        'M' => 22,
        'N' => 23,
        'O' => 24,
        'P' => 25,
        'Q' => 26,
        'R' => 27,
        'S' => 28,
        'T' => 29,
        'U' => 30,
        'V' => 31,
        'W' => 32,
        'X' => 33,
        'Y' => 34,
        'Z' => 35,
    ];

    /**
     * Weights to calculate control sum
     * @var array
     */
    private const WEIGHTS = [
        1, 10, 3, 30, 9, 90, 27, 76, 81, 34, 49, 5, 50, 15, 53, 45, 62, 38, 89, 17, 73, 51, 25, 56, 75, 71, 31, 19, 93, 57
    ];

    /**
     * Two letters country code as in ISO 3166-1 alpha-2 standard
     * @var string
     */
    protected $country;

    /**
     * Country specific IBAN exact length
     * @var int
     */
    protected $length;

    /**
     * Bank account number to validate
     * @var string
     */
    private $number;

    public function __construct(string $number)
    {
        if (empty($this->country)) {
            throw new LogicException('Country code not provided');
        }

        if (empty($this->length)) {
            throw new LogicException('Country specific length not provided');
        }

        $this->number = \str_replace(' ', '', \strtoupper($number));

        if (\substr($this->number, 0, 2) !== $this->country) {
            $this->number = $this->country . $this->number;
        }
    }

    public function validate(): bool
    {
        if (!$this->validateLength()) {
            return false;
        }

        // This order of below operations must be preserved
        $this->moveFourInitialCharsToEnd();
        $this->replaceLettersToDigits();

        return $this->computeAndCheckControlSum();
    }

    private function validateLength(): bool
    {
        return \strlen($this->number) === $this->length;
    }

    private function moveFourInitialCharsToEnd(): void
    {
        $this->number = \substr($this->number, 4) . \substr($this->number, 0, 4);
    }

    private function replaceLettersToDigits(): void
    {
        foreach (\array_keys(self::LETTERS_CODES) as $letter) {
            $this->number = \str_replace($letter, self::LETTERS_CODES[$letter], $this->number);
        }
    }

    private function computeAndCheckControlSum(): bool
    {
        $length = \strlen($this->number);
        $controlSum = 0;
        for ($i = 0; $i < $length; $i++) {
            $controlSum += $this->number[$length - 1 - $i] * self::WEIGHTS[$i];
        }

        return $controlSum % 97 === 1;
    }
}
