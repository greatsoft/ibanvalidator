<?php

namespace Validator\IBAN\DE;

use Validator\IBAN\AbstractIbanValidator;

/**
 * German bank account number validator
 */
final class IbanValidatorDe extends AbstractIbanValidator
{
    /**
     * Two letters country code as in ISO 3166-1 alpha-2 standard
     * @var string
     */
    protected $country = 'DE';

    /**
     * Country specific IBAN exact length
     * @var int
     */
    protected $length = 22;
}
