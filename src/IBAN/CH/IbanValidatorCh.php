<?php

namespace Validator\IBAN\CH;

use Validator\IBAN\AbstractIbanValidator;

/**
 * Swiss bank account number validator
 */
final class IbanValidatorCh extends AbstractIbanValidator
{
    /**
     * Two letters country code as in ISO 3166-1 alpha-2 standard
     * @var string
     */
    protected $country = 'CH';

    /**
     * Country specific IBAN exact length
     * @var int
     */
    protected $length = 21;
}
