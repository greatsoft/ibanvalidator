<?php

namespace Validator\Tests\IBAN\UK;

use PHPUnit\Framework\TestCase;
use Validator\IBAN\UK\IbanValidatorUk;

final class IbanValidatorUkTest extends TestCase
{
    /**
     * @dataProvider ibans
     */
    public function testValidate(string $iban, bool $isValid)
    {
        $validator = new IbanValidatorUk($iban);
        $this->assertSame($isValid, $validator->validate());
    }

    public function ibans(): array
    {
        // [IBAN, isValid]
        return [
            ['GB29 NWBK 6016 1331 9268 19', true],
            ['29 NWBK 6016 1331 9268 19', true],
            ['GB29 NWBK 6016 1331 9268 20', false],
            ['29 NWBK 6016 1331 9268 20', false],
        ];
    }
}
