<?php

namespace Validator\Tests\IBAN\UK;

use PHPUnit\Framework\TestCase;
use Validator\IBAN\IbanValidatorFactory;
use Validator\IBAN\CH\IbanValidatorCh;
use Validator\IBAN\DE\IbanValidatorDe;
use Validator\IBAN\PL\IbanValidatorPl;
use Validator\IBAN\UK\IbanValidatorUk;
use LogicException;

final class IbanValidatorFactoryTest extends TestCase
{
    /** @var  IbanValidatorFactory */
    private $factory;

    public function setUp()
    {
        $this->factory = new IbanValidatorFactory();
    }

    /**
     * @dataProvider makeSuccessDataProvider
     */
    public function testMakeSuccess(string $countryCode, string $className)
    {
        $validator = $this->factory->make($countryCode, '012345');
        $this->assertInstanceOf($className, $validator);
    }

    public function makeSuccessDataProvider(): array
    {
        return [
            ['ch', IbanValidatorCh::class],
            ['DE', IbanValidatorDe::class],
            ['PL', IbanValidatorPl::class],
            ['UK', IbanValidatorUk::class],
        ];
    }

    /**
     * @expectedException LogicException
     */
    public function testMakeFailure()
    {
        $countryCode = 'Unknown';
        $this->factory->make($countryCode, '012345');
    }
}
