<?php

namespace Validator\IBAN\PL;

use Validator\IBAN\AbstractIbanValidator;

/**
 * Polish bank account number validator
 */
final class IbanValidatorPl extends AbstractIbanValidator
{
    /**
     * Two letters country code as in ISO 3166-1 alpha-2 standard
     * @var string
     */
    protected $country = 'PL';

    /**
     * Country specific IBAN exact length
     * @var int
     */
    protected $length = 28;
}
