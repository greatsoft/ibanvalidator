<?php

namespace Validator\Tests\IBAN\CH;

use PHPUnit\Framework\TestCase;
use Validator\IBAN\CH\IbanValidatorCh;

final class IbanValidatorChTest extends TestCase
{
    /**
     * @dataProvider ibans
     */
    public function testValidate(string $iban, bool $isValid)
    {
        $validator = new IbanValidatorCh($iban);
        $this->assertSame($isValid, $validator->validate());
    }

    public function ibans(): array
    {
        // [IBAN, isValid]
        return [
            ['93 0076 2011 6238 5295 7', true],
            ['CH93 0076 2011 6238 5295 7', true],
            ['AH93 0076 2011 6238 5295 7', false],
            ['93 0076 2011 6238 5295', false],
            ['93 0076 2011 6238 5295 77', false],
            ['CH93 0076 2011 6238 5295 5', false],
        ];
    }
}
