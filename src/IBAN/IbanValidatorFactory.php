<?php

namespace Validator\IBAN;

use LogicException;
use Validator\IBAN\CH\IbanValidatorCh;
use Validator\IBAN\DE\IbanValidatorDe;
use Validator\IBAN\PL\IbanValidatorPl;
use Validator\IBAN\UK\IbanValidatorUk;

/**
 * IBAN validator factory class
 */
final class IbanValidatorFactory
{
    /**
     * Method builds and returns concrete IBAN validator
     *
     * @param string $countryCode   Two letters country code as in ISO 3166-1 alpha-2 standard
     * @param string $iban          International bank account number to validate
     *
     * @throws LogicException
     *
     * @return IbanValidatorInterface
     */
    public function make(string $countryCode, string $iban): IbanValidatorInterface
    {
        $countryCode = \strtoupper($countryCode);

        switch ($countryCode) {
            case 'CH':
                return new IbanValidatorCh($iban);
            case 'DE':
                return new IbanValidatorDe($iban);
            case 'PL':
                return new IbanValidatorPl($iban);
            case 'UK':
                return new IbanValidatorUk($iban);
            default:
                $message = sprintf('IBAN validator for %s not implemented yet.', $countryCode);
                throw new LogicException($message);
        }
    }
}
