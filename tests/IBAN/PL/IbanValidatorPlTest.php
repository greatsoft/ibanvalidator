<?php

namespace Validator\Tests\IBAN\PL;

use PHPUnit\Framework\TestCase;
use Validator\IBAN\PL\IbanValidatorPl;

final class IbanValidatorPlTest extends TestCase
{
    /**
     * @dataProvider ibans
     */
    public function testValidate(string $iban, bool $isValid)
    {
        $validator = new IbanValidatorPl($iban);
        $this->assertSame($isValid, $validator->validate());
    }

    public function ibans(): array
    {
        // [IBAN, isValid]
        return [
            ['pl61 1090 1014 0000 0712 1981 2874', true],
            ['61 1090 1014 0000 0712 1981 2874', true],
            ['PL71 1090 1014 0000 0712 1981 2874', false],
            ['81 1090 1014 0000 0712 1981 2874', false]
        ];
    }
}
