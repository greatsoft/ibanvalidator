<?php

namespace Validator\IBAN\UK;

use Validator\IBAN\AbstractIbanValidator;

/**
 * United Kingdom bank account number validator
 */
final class IbanValidatorUk extends AbstractIbanValidator
{
    /**
     * Two letters country code as in ISO 3166-1 alpha-2 standard
     * @var string
     */
    protected $country = 'GB';

    /**
     * Country specific IBAN exact length
     * @var int
     */
    protected $length = 22;
}
