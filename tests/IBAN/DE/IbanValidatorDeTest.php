<?php

namespace Validator\Tests\IBAN\DE;

use PHPUnit\Framework\TestCase;
use Validator\IBAN\DE\IbanValidatorDe;

final class IbanValidatorDeTest extends TestCase
{
    /**
     * @dataProvider ibans
     */
    public function testValidate(string $iban, bool $isValid)
    {
        $validator = new IbanValidatorDe($iban);
        $this->assertSame($isValid, $validator->validate());
    }

    public function ibans(): array
    {
        // [IBAN, isValid]
        return [
            ['DE89 3704 0044 0532 0130 00', true],
            ['89 3704 0044 0532 0130 00', true],
            ['DE 89 3704 0044 0532 0130 00', true],
            ['DE 89 3704 0044 0532 0130 01', false],
            ['', false],
            ['89 3704 0044 0532 0130 01', false],
        ];
    }
}
