<?php

namespace Validator\IBAN;

interface IbanValidatorInterface
{
    public function validate(): bool;
}
